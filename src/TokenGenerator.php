<?php

namespace Token;

/**
 * Class TokenGenerator.
 */
class TokenGenerator
{
    /**
     * @var \DateTime
     */
    protected $date;

    /**
     * @var string
     */
    protected $secret = '';

    /**
     * @var mixed
     */
    protected $content = '';

    /**
     * @var string
     */
    protected $contentType = '';

    /**
     * @var string
     */
    protected $uri = '';

    /**
     * @var string
     */
    protected $method = '';

    /**
     * @var string
     */
    protected $clientId = '';

    /**
     * Generate authorization token.
     *
     * @return string
     */
    public function generateToken(): string
    {
        // Get raw token string
        $tokenRawData = $this->getRawToken();

        // Hash the token raw data.
        $hmac = $this->getHmacSha256($tokenRawData);

        // Make it to base64.
        $token = $this->convertToBase64($hmac);

        return sprintf('%s:%s', $this->getClientId(), $token);
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     *
     * @return TokenGenerator
     */
    public function setDate(\DateTime $date): TokenGenerator
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     *
     * @return TokenGenerator
     */
    public function setContent($content): TokenGenerator
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     *
     * @return TokenGenerator
     */
    public function setUri(string $uri): TokenGenerator
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return strtolower($this->method);
    }

    /**
     * @param string $method
     *
     * @return TokenGenerator
     */
    public function setMethod(string $method): TokenGenerator
    {
        $this->method = $method;

        return $this;
    }

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }

    /**
     * @param string $clientId
     *
     * @return TokenGenerator
     */
    public function setClientId($clientId): TokenGenerator
    {
        $this->clientId = $clientId;

        return $this;
    }

    /**
     * @return string
     */
    public function getContentType(): string
    {
        return strtolower($this->contentType);
    }

    /**
     * @param string $contentType
     *
     * @return TokenGenerator
     */
    public function setContentType(string $contentType): TokenGenerator
    {
        $this->contentType = $contentType;

        return $this;
    }

    /**
     * @return string
     */
    public function getSecret(): string
    {
        return $this->secret;
    }

    /**
     * @param string $secret
     *
     * @return TokenGenerator
     */
    public function setSecret(string $secret): TokenGenerator
    {
        $this->secret = $secret;

        return $this;
    }

    /**
     * Get raw token string.
     *
     * @return string
     */
    private function getRawToken(): string
    {
        return sprintf(
            '%s:%s:%s:%s:%s',
            $this->getContentType(),
            $this->getMethod(),
            $this->getContent(),
            $this->getUri(),
            $this->getDateAsUtcString()
        );
    }

    /**
     * Get HMACK-SHA256 hash.
     *
     * @param string $tokenRawData
     *
     * @return string
     */
    private function getHmacSha256($tokenRawData): string
    {
        return hash_hmac('sha256', $tokenRawData, $this->getSecret(), true);
    }

    /**
     * Convert to Base64 string.
     *
     * @param string $text
     *
     * @return string
     */
    private function convertToBase64($text): string
    {
        return base64_encode($text);
    }

    /**
     * Convert date to utc and format to ISO8601.
     *
     * @return string
     */
    private function getDateAsUtcString(): string
    {
        $date = $this->getDate();
        if ($date instanceof \DateTime) {
            $date->setTimezone(new \DateTimeZone('UTC'));

            return $date->format(\DateTime::ISO8601);
        }

        return '';
    }
}
