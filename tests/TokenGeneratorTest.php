<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Token\TokenGenerator;

/**
 * @covers \Token\TokenGenerator
 */
class TokenGeneratorServiceTest extends TestCase
{
    /**
     * @var TokenGenerator
     */
    private $tokenGenerator;

    /**
     * @var \DateTime
     */
    private $date;

    const CLIENT_ID = 't1st1n6';
    const CLIENT_SECRET = 'super_secret_string';
    const CONTENT_TYPE = 'json';
    const METHOD = 'post';
    const URI = 'http://localhost';
    const CONTENT = 'content';

    public function setUp()
    {
        parent::setUp();

        $this->date = new \DateTime('1992-08-23 12:12:12');
        $this->tokenGenerator = new TokenGenerator();
    }

    public function testAllParamsMissing()
    {
        $token = $this->tokenGenerator->generateToken();
        list($clientId, $token) = explode(':', $token);

        $this->assertSame('g0PAhnFKmVACa9xtDBlfzuMUH3sLHt37Rlu4/aKDB2s=', $token);
        $this->assertSame('', $clientId);
    }

    public function testWithClientId()
    {
        $token = $this->tokenGenerator->setClientId(self::CLIENT_ID)->generateToken();
        list($clientId, $token) = explode(':', $token);

        $this->assertSame('g0PAhnFKmVACa9xtDBlfzuMUH3sLHt37Rlu4/aKDB2s=', $token);
        $this->assertSame(self::CLIENT_ID, $clientId);
        $this->assertSame(self::CLIENT_ID, $this->tokenGenerator->getClientId());
    }

    public function testWithSecret()
    {
        $token = $this->tokenGenerator
            ->setClientId(self::CLIENT_ID)
            ->setSecret(self::CLIENT_SECRET)
            ->generateToken();

        list($clientId, $token) = explode(':', $token);

        $this->assertSame('u25qEZyzwZE7WsfaU0dQqVsYMbDU3EgODEyUcJrdDyQ=', $token);
        $this->assertSame(self::CLIENT_ID, $clientId);
        $this->assertSame(self::CLIENT_ID, $this->tokenGenerator->getClientId());
        $this->assertSame(self::CLIENT_SECRET, $this->tokenGenerator->getSecret());
    }

    public function testWithContentType()
    {
        $token = $this->tokenGenerator
            ->setClientId(self::CLIENT_ID)
            ->setSecret(self::CLIENT_SECRET)
            ->setContentType(self::CONTENT_TYPE)
            ->generateToken();

        list($clientId, $token) = explode(':', $token);

        $this->assertSame('KdplPf6IKu+pG7dxOyUJUgUjtBgBqC8Vlod2c4bEOGI=', $token);
        $this->assertSame(self::CLIENT_ID, $clientId);
        $this->assertSame(self::CLIENT_ID, $this->tokenGenerator->getClientId());
        $this->assertSame(self::CLIENT_SECRET, $this->tokenGenerator->getSecret());
        $this->assertSame(self::CONTENT_TYPE, $this->tokenGenerator->getContentType());
    }

    public function testWithDate()
    {
        $token = $this->tokenGenerator
            ->setClientId(self::CLIENT_ID)
            ->setSecret(self::CLIENT_SECRET)
            ->setContentType(self::CONTENT_TYPE)
            ->setDate($this->date)
            ->generateToken();

        list($clientId, $token) = explode(':', $token);

        $this->assertSame('HWZjWhI3kQMbnnG5Nu8yzIkeJYE/A5NYD7QA9leYH80=', $token);
        $this->assertSame(self::CLIENT_ID, $clientId);
        $this->assertSame(self::CLIENT_ID, $this->tokenGenerator->getClientId());
        $this->assertSame(self::CLIENT_SECRET, $this->tokenGenerator->getSecret());
        $this->assertSame($this->date, $this->tokenGenerator->getDate());
    }

    public function testWithMethod()
    {
        $token = $this->tokenGenerator
            ->setClientId(self::CLIENT_ID)
            ->setSecret(self::CLIENT_SECRET)
            ->setContentType(self::CONTENT_TYPE)
            ->setDate($this->date)
            ->setMethod(self::METHOD)
            ->generateToken();

        list($clientId, $token) = explode(':', $token);

        $this->assertSame('GJFlUqEpY/DkHpIcEZICVMgQDvhxWmWUxHJrrK6JR68=', $token);
        $this->assertSame(self::CLIENT_ID, $clientId);
        $this->assertSame(self::CLIENT_ID, $this->tokenGenerator->getClientId());
        $this->assertSame(self::CLIENT_SECRET, $this->tokenGenerator->getSecret());
        $this->assertSame($this->date, $this->tokenGenerator->getDate());
        $this->assertSame(self::METHOD, $this->tokenGenerator->getMethod());
    }

    public function testWithUri()
    {
        $token = $this->tokenGenerator
            ->setClientId(self::CLIENT_ID)
            ->setSecret(self::CLIENT_SECRET)
            ->setContentType(self::CONTENT_TYPE)
            ->setDate($this->date)
            ->setMethod(self::METHOD)
            ->setUri(self::URI)
            ->generateToken();

        list($clientId, $token) = explode(':', $token);

        $this->assertSame('rcyU45tfNtlwYOmR9VGMHZs+L4PVzmcgvZsDA2DVVC8=', $token);
        $this->assertSame(self::CLIENT_ID, $clientId);
        $this->assertSame(self::CLIENT_ID, $this->tokenGenerator->getClientId());
        $this->assertSame(self::CLIENT_SECRET, $this->tokenGenerator->getSecret());
        $this->assertSame($this->date, $this->tokenGenerator->getDate());
        $this->assertSame(self::METHOD, $this->tokenGenerator->getMethod());
        $this->assertSame(self::URI, $this->tokenGenerator->getUri());
    }

    public function testWithContent()
    {
        $token = $this->tokenGenerator
            ->setClientId(self::CLIENT_ID)
            ->setSecret(self::CLIENT_SECRET)
            ->setContentType(self::CONTENT_TYPE)
            ->setDate($this->date)
            ->setMethod(self::METHOD)
            ->setUri(self::URI)
            ->setContent(md5(json_encode(self::CONTENT)))
            ->generateToken();

        list($clientId, $token) = explode(':', $token);

        $this->assertSame('551vW37UGDRYE2NcJotscO25pEm8H+jHwx1RHFuzO0w=', $token);
        $this->assertSame(self::CLIENT_ID, $clientId);
        $this->assertSame(self::CLIENT_ID, $this->tokenGenerator->getClientId());
        $this->assertSame(self::CLIENT_SECRET, $this->tokenGenerator->getSecret());
        $this->assertSame($this->date, $this->tokenGenerator->getDate());
        $this->assertSame(self::METHOD, $this->tokenGenerator->getMethod());
        $this->assertSame(self::URI, $this->tokenGenerator->getUri());
        $this->assertSame(md5(json_encode(self::CONTENT)), $this->tokenGenerator->getContent());
    }
}