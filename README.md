Token generator
====

Helper class for creating tokens for api request.

Features
--------

 * single helper class to create token from api request data
 
Documentation
-------------

Token is generated from the following data:
Content-Type (json etc.)
Request method (POST, GET, PUT, DELETE)
Request content (md5 hashed json string)
Request uri (API uri)
Date (Request date)
Client secret
Client ID

For generating hash content type, method, content, uri, date in utc (ISO8601 => Y-m-d\TH:i:sO) are glued together (order is important).

Glued string is then hmac hashed in sha256 with client secret and that hashed string is then base64 encoded.

At the very end base64 string is glued together with client ID like: clientID:base64String

Usage
-----

```php
<?php
 
use Token\TokenGenerator;
 
$date = new \DateTime(null, new \DateTimeZone('UTC'));
$url = 'http://example.com';

$t = (new TokenGenerator())
    ->setClientId('t1st1n6')
    ->setContentType('json')
    ->setMethod('GET')
    ->setUri($url)
    ->setDate($date)
    ->setContent(md5(null))
    ->setSecret('super_secret_string');
$toke = $t->generateToken();
 
?>
```

Installation
------------

Add private repository to your project composer.json

```json
{
    "require": {
        "php/token-generator": "1.0.0"
    },
    "repositories": [
        {
            "type": "vcs",
            "url":  "https://gitlab.finestmedia.ee/php/token-generator.git"
        }
    ]
}
```


Install using [composer](https://getcomposer.org/download/): `composer.phar install`

Testing
-------

run `phpunit` in the root directory of the library for the full
test suite.

License
-------

This library is under the [MIT license](LICENSE).
